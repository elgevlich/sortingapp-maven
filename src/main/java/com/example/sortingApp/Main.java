package com.example.sortingApp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final Logger logger = LogManager.getLogger(Sorting.class);
    public static void main(String[] args) {


        if (args == null){
            throw new IllegalArgumentException("Argument cannot be null");

        } else {
            try {
                Sorting.sort(args);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Argument cannot be non digital");
            }
        }

        logger.debug("I am using logging!");
    }
}
