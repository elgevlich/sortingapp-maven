package com.example.sortingApp;

import java.util.Arrays;

public class Sorting {


    public static void sort(String[] array) {

        if (array.length > 10) {
            throw new IllegalArgumentException("More than ten arguments");
        }
        if (array.length != 0) {

            int[] arrayInt = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                arrayInt[i] = Integer.parseInt(array[i]);
            }
            Arrays.sort(arrayInt);
            for (int i = 0; i < array.length; i++) {
                array[i] = String.valueOf(arrayInt[i]);
            }


        }
    }


}


