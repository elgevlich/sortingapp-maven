import com.example.sortingApp.Main;
import com.example.sortingApp.Sorting;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;



@RunWith(Parameterized.class)
public class SortingTest {


    @Test(expected = IllegalArgumentException.class)
    public void NullCase() {

        Main.main(null);

    }

    @Test(expected = IllegalArgumentException.class)
    public void OverTenArgCase() {
        String[] array = {"1", "4", "3", "2", "1", "1", "4", "3", "2", "1", "6"};
        Main.main(array);

    }

    @Test(expected = IllegalArgumentException.class)
    public void NonDigitalArgCase() {
        String[] array = {"23no"};
        Main.main(array);

    }

    @Test
    public void testEmptyCase() {
        String[] a = {};
        String[] expected = {};
        Main.main(a);
        assertArrayEquals(expected, a);
    }

    @Test
    public void testSingleElementArrayCase() {
        String[] a = {"12"};
        String[] expected = {"12"};
        Main.main(a);
        assertArrayEquals(expected, a);
    }

    @Test
    public void testSortedArraysCase() {
        String[] a = {"1", "12", "45"};
        String[] expected = {"1", "12", "45"};
        Main.main(a);
        assertArrayEquals(expected, a);

    }

    @Test
    public void testOtherCases() {
        String[] a = {"12", "1", "45"};
        String[] expected = {"1", "12", "45"};
        Main.main(a);
        assertArrayEquals(expected, a);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        String[] in = {"2","6","4"};
        String[] ex = {"2","4","6"};

        String[] in1 = {"-2","-10","-2", "34", "15"};
        String[] ex1 = {"-10","-2","-2", "15", "34"};


        String[] in2 = {"4","1","4"};
        String[] ex2 = {"1","4","4"};

        String[] in3 = {"-4","-100","0"};
        String[] ex3 = {"-100","-4","0"};

        String[] in4 = {"-4", "-100", "0", "5", "4", "6", "9", "8", "7", "10" };
        String[] ex4 = {"-100", "-4", "0", "4", "5", "6", "7", "8", "9", "10" };


        return Arrays.asList(new Object[][]{

                {in, ex},
                {in1, ex1},
                {in2, ex2},
                {in3, ex3},
                {in4, ex4}

        });
    }
    private String[] input;
    private String[] expected;

    public SortingTest(String[] input, String[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void parametrizedTest() {
        Main.main(input);
        assertArrayEquals(expected, input);
    }

}



